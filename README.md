# FS Node Server <!-- omit in toc -->

- [Description](#description)
- [Installation](#installation)
- [Usage](#usage)
- [Routes](#routes)

## Description

Node API server that performs various FS operations on a server.

## Installation

Install Packages

```bash
npm install
```

## Usage

Start Dev Server

```bash
npm run dev
```

## Routes

To check if path exists, send a GET request to the following api. GET Request body must be in JSON format with the "path" key.

`http://localhost:3000/fsapi/check-path/`

### Example <!-- omit in toc -->

```http
GET http://localhost:3000/fsapi/check-path/
Content-Type: application/json

{
    "path":"C:/Temp"
}
```
