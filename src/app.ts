// Imports
import express, {
  Application,
  Request,
  Response,
  NextFunction,
  Router,
} from "express";

// Create an Instance of the Express Top Level Function
const app: Application = express();
// Specifies the port the express server will run on
const port = 3000;

// Configure server to accept JSON
// express.json() is middleware that parses requests with JSON body data.
app.use(express.json());

// Define router middleware
const fsApiRouter: Router = require("./routes/fsapi");

//Endpoint for api that sends requests to the fsApiRouter middleware that was defined above
//Example endpoint http://localhost/3000/fsapi/
app.use("/fsapi", fsApiRouter);

// Binds host and port and listens for connections.
app.listen(port, (): void => {
  return console.log(`Express is listening at http://localhost:${port}`);
});
