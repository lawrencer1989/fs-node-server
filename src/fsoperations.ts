import { pathExists } from "fs-extra";

//Async Function to check path. Returns a Promise
export async function checkPathExist(filePath: string): Promise<Boolean> {
  const found: Boolean = await pathExists(filePath);
  return found;
}

// TODO: Implement A Function to create a directory. FS-Extra methods for creating a direectory automatically checks if path exists and creates thee path if it does not
