// Imports the express package
import express, { Request, Response, NextFunction } from "express";
import { checkPathExist } from "../fsoperations";
const router = express.Router();

// TODO: Implement a middleware function to check for specific JSON Keys. This would alleviate the need to rewrite the same code for different routes.

//Check Path Exists Route. Requires the Body of the request to be JSON
router.get(
  "/check-path",
  async (req: Request, res: Response, next: NextFunction) => {
    //Checks if JSON Request body has "path" using the hasOwnProperty() builtin js function
    if (req.body.hasOwnProperty("path")) {
      var path: string = req.body.path;
      const checkPath = await checkPathExist(path);
      if (checkPath) {
        console.log(`Path: ${path} - Exists`);
      } else {
        console.error(`Path: ${path} - Does Not Exist`);
      }
    } else {
      console.error("Path Key Missing");
    }
  }
);

module.exports = router;
